# Image Stitching using DLT (Direct Linear Transform)

## Table of Contents
- [Introduction](#introduction)
  - [Part 1: 4-Point Homography](#part-1-4-point-homography)
  - [Part 2: 8-Point Homography](#part-2-8-point-homography)
  - [Part 3: Panorama](#part-3-panorama)
- [Results](#results)


## Introduction

This repository contains code and instructions for image stitching using the Direct Linear Transform (DLT) technique. The goal is to create panoramic images by aligning and combining multiple source images.

## Getting Started

To get started with this Homework, follow the instructions for each part:

### Part 1: 4-Point Homography

In this part, we replace four points in the source image with their corresponding points in the destination image. The steps involved are as follows:

1. Run `select_points(im, points)` to manually select four corresponding points.
2. Use the `compute_homography(pts_src, pts_dest)` function to compute the homography matrix using the DLT four-point theorem.
3. Warp the perspective of the source image to match the destination image using `warp_image(image1, image2)`.
4. Combine the source and destination images using `combine_images(src, dest, mask)`.

### Part 2: 8-Point Homography

Similar to Part 1, this part involves selecting eight corresponding points to create a more accurate alignment between images. While it can be challenging to find matching points, selecting the right ones is crucial for accurate correspondence.

### Part 3: Panorama

In the final part, we create a panorama by stitching together multiple images. The process includes:

1. Loading all images to be stitched using `process_images_and_save(source_dir, output_dir, padding_factors)` to ensure they have the same aspect ratio.
2. Storing handpicked corresponding points in numpy arrays.
3. Computing homographies for image alignment.
4. Using `combine_images(H, im1, warped, masked, combined)` to combine two images at a time to create the panorama.

## Usage

To use this Homework, follow these general steps:

1. Clone the repository to your local machine.
2. Install opencv, matplotlib, PIL, numpy
3. Run the code for each part as described in the individual sections and instructions in the notebook.
4. Review the results in the "OUTPUT IMAGES" folder.


## Results

4-Point Homography
![Alt Text](photo_replace_4pts.jpg)

8-Point Homography on a portrait
![Alt Text](photo_replace_8pts.jpg)

Panaroma
![Alt Text](Pan/combined1_5432.jpg)




