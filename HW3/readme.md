# Image Registration using Factor Graph

## Table of Contents

1. [Introduction](#introduction)
2. [Getting Started](#getting-started)
   - [Part 1: image_registration_one](#part-1-image_registration_one)
   - [Part 2: image_registration_two](#part-2-image_registration_two)
   - [Part 3: image_registration_three](#part-3-image_registration_three)
3. [Results](#results)

## Introduction

Welcome to the Image Registration project using factor graphs and LM optimization. This repository contains code and instructions for aligning and stitching images together using factor graphs.

## Getting Started

To get started with this project, follow the instructions for each part:

### Part 1: image_registration using SIFT detectors

In this part, you'll perform image preprocessing and feature detection using SIFT feature detectors.

- Use the `Skerki_Dataset` class to apply preprocessing (normalization) on the images.
- Utilize the `FeatureDetection` class to detect features using SIFT feature detectors and display the images with detected features.
- Use the `DrawInliers` class to find inliers from the selected keypoints, borrowed from `zernike_py`.
- Implement functions like `getHomography` to find homographies, `alpha_blending`, `warpTwoImages`, and `create_mosaic` to stitch the images together.

### Part 2: image_registration using Factor Graphs (6 images)

In this part, you'll focus on adding noise to the pose, managing similarity matrices, and extracting initial poses based on image homography.

- Introduce the concept of `ODOMETRY_NOISE` to add noise to the pose.
- Utilize `getFactorPose` to add poses based on similarity matrices calculated using `cv2.estimateAffinePartial2D`.
- Implement `extractHomographyPose` to add initial poses based on the homography of the images.
- Use `plot_graph` to visualize the factor graph.

### Part 3: image_registration_three
- same as part 2, added funtionality for loop closure based on if homography is good

## Results

PART 1a
![stiched image](results/part_1a.png)


PART 1a
![stiched image](results/part_1b.png)

PART 2
![Factor Graph 6 Images](results/part2.png)

PART 2
![Factor Graph 29 Images](results/part3.png)

