# EECE 7150

Course Work for EECE 7150, Autonomous Field Robotics

## Structure:

- rlabbe - bayesian filters in Python tutorials
- books - Books regarding coursework
- papers - research papers regarding coursework
- HW1 (Markov Chain - Simulation)
    - assignment-1 - jupyter notebook, with writeup and code
- HW2 (Image Stiching - DLT)
    - assignment-2 - jupyter notebook, with writeup and code, and Output images 
- HW3 (large Scale Mosiacing for underwater images based on GTSAM)
    - assignment-3 - jupyter notebook, with writeup and factor graph
- HW4 (Incremental Structure for Motion for images)
    - assignment-4 - jupyter notebook, with python files for the pipeline, along with the Point Cloud
